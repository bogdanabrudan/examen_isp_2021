package s2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class s2 implements ActionListener {
    JFrame frame;
    JButton button;
    JPanel panel;
    JTextField t1;
    JTextField t2;
    JTextArea textArea;

    public s2(){
        frame = new JFrame();
        t1 = new JTextField();
        t2 = new JTextField();
        textArea = new JTextArea("The result");
        textArea.setEditable(false);
        button = new JButton("Calculate");
        button.addActionListener(this);
        panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));
        panel.setLayout(new GridLayout(0, 1));
        panel.add(t1);
        panel.add(t2);
        panel.add(button);
        panel.add(textArea);
        frame.add(panel, BorderLayout.CENTER);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new s2();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int a = Integer.parseInt((String) t1.getText());
        int b = Integer.parseInt((String) t2.getText());
        int c = a + b;
        textArea.setText(Integer.toString(c));
    }
}

